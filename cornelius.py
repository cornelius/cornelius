#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Cornelius fetch an url, looks in html data all sub-urls and show
information about each of them, like cache age, size

"""
import sys
import os
from optparse import OptionParser
from BeautifulSoup import BeautifulSoup
import urllib2
import time
import operator

__version__ = "1.1.0"

def parse_file(fpath):
    """
    Open a file, return body
    """
    html = open(fpath,'r').read()
    return html

def parse(html):
    """ Parse html document, returns all url in a dict"""
    images = []
    scripts = []
    styles = []
    # create a beautiful soup object
    soup = BeautifulSoup(html)    
    # look for images
    for image in soup.findAll("img"):
        images.append(image['src'])

    for image in soup.findAll("script"):
        try:
            scripts.append(image['src'])
        except KeyError:
            pass

    for styl in soup.findAll("link"):
        try: 
            rel = styl['rel'] 
            href = styl['href'] 
            if href and rel == 'stylesheet':
                styles.append(href)
        except: 
            pass


    for css in soup.findAll("style"):
        styles.append(css_import(css.contents[0]))

    return [list(set(images)), list(set(scripts)), list(set(styles))]

def css_import(text):
    """Grab url in import css rules"""
    url = text.split('(')[1]
    url = url.split(')')[0]
    return url


def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-i FILENAME] [-u URL] [-avs]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-a", "--all", dest="exclusive",
                      action="store_false",
                      help="check all urls", 
                      default=True)
    parser.add_option("-i", "--infile", dest="infile",
                      help="input file", 
                      default=None)
    parser.add_option("-u", "--url", dest="url",
                      help="url to fetch and analyse", 
                      default=None)
    parser.add_option("--hostname", dest="hostname",
                      help="hostname like http://fqdn/", 
                      default=None)
    parser.add_option("--username", dest="username",
                      help="username for HTTP auth", 
                      default=None)
    parser.add_option("-O", "--outfile", dest="outfile",
                      help="dumpt html in outfile", 
                      default=None)
    parser.add_option("--password", dest="password",
                      help="password for HTTP auth", 
                      default=None)
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose", 
                      default=False)
    parser.add_option("-s", "--show-size", dest="show_size",
                      action="store_true",
                      help="show the size on output", 
                      default=False)

    return parser.parse_args()[0]

def check_options(options):
    """Check mandatory options"""
    if options.infile == None and options.url == None:
        print """Input file or url is required, use -i or -u on command line"""
        sys.exit(1)

    if options.infile != None and options.hostname == None:
        print """You must add --hostname option when using input file"""
        sys.exit(1)

    if not options.hostname.startswith('http://') and not options.hostname.startswith('https://'):
        print """Hostname must start with http or https"""
        sys.exit(2)

def urlparse(url, credentials):
    """
    open an url and return information
    """
    username, password = credentials
    if username != None and password != None:
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, url, username, password)
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        # create "opener" (OpenerDirector instance)
        opener = urllib2.build_opener(handler)
        opener.open(url)
        urllib2.install_opener(opener)

    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError, err:
        print err

    return response.info()

def parse_url(url, credentials):
    """Parse an online document"""
    print "Fetch url : %s" % url
    html = urlfetch(url, credentials)
    if html == None:
        print 'No html code to analyze'
        sys.exit(4)
    return html

def urlfetch(url, credentials):
    """
    Fetch an url, return body part
    """
    username, password = credentials

    if username != None and password != None:
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, url, username, password)
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        # create "opener" (OpenerDirector instance)
        opener = urllib2.build_opener(handler)
        opener.open(url)
        urllib2.install_opener(opener)
    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError, error:
        print error
        sys.exit(1)
    try:
        html = response.read()
    except:
        html = None
    return html

def age_in_hdr(header):
    """Return age header"""
    try:
        age = header.getheader('Age')
    except:
        age = "Not set"
    return str(age)

def length_in_hdr(header):
    """Return age header"""
    try:
        age = header.getheader('Content-Length')
    except:
        pass
    try:
        age = int(age)
    except:
        age = 0
    return age

def pretty_size(size):
    """Return age header"""
    size_bytes = float(size)
    if size_bytes >= 1099511627776:
        terabytes = size_bytes / 1099511627776
        size = '%.2fT' % terabytes
    elif size_bytes >= 1073741824:
        gigabytes = size_bytes / 1073741824
        size = '%.2fG' % gigabytes
    elif size_bytes >= 1048576:
        megabytes = size_bytes / 1048576
        size = '%.2fM' % megabytes
    elif size_bytes >= 1024:
        kilobytes = size_bytes / 1024
        size = '%.2fK' % kilobytes
    else:
        size = '%.0fb' % size_bytes
    return size


def sort_urls(urls):
    """Sort url by type

    Output a dict with img, js, css, kss in this order
    """
    images = []
    javasc = []
    styles = []
    others = []

    for url in urls:
        if url.endswith('.png'):
            images.append(url)
        elif url.endswith('.gif'):
            images.append(url)
        elif url.endswith('.jpg'):
            images.append(url)
        elif url.endswith('.jpeg'):
            images.append(url)
        elif url.endswith('.js'):
            javasc.append(url)
        elif url.endswith('.css'):
            styles.append(url)
        elif url.endswith('.kss'):
            styles.append(url)
        else:
            others.append(url)

    return images + javasc + styles + others

def completeurl(ori, hostname):
    """Complete non fqdn url"""
    if not hostname.endswith('/'):
        hostname = hostname + '/'

    if ori.startswith(hostname):
        url = ori
    else:
        if not ori.startswith('http'):
            if ori.startswith('/'):
                url = hostname + ori[1:]
            else:
                url = hostname + ori
        else:
            url = ori

    return url

def analyze_urls(urls, hostname, credentials, exmode, part=None):
    """Analyze an url"""
    total_cache = 0
    total_no_cache = 0
    for ori in urls:
        url = completeurl(ori, hostname)
        if not exmode or ( exmode and url.startswith(hostname) ):
            cache, no_cache = analyze_url(url, hostname, credentials, part)
            total_cache = total_cache + cache
            total_no_cache = total_no_cache + no_cache
    return total_cache, total_no_cache

def analyze_url(url, hostname, credentials, part=None):
    """Analyze an url"""
    total_cache = 0
    total_no_cache = 0
    header = urlparse(url, credentials)
    url_age = age_in_hdr(header)
    url_length = length_in_hdr(header)
    try:
        age_str = time.strftime('%H:%M:%S', time.gmtime(float(url_age)))
        total_cache = url_length
    except:
        age_str = '-'
        total_no_cache = url_length
    print "%10s %8d %8s %6s %s" % (age_str,  url_length, url_age, part, url)

    return total_cache, total_no_cache

def hosts_stats(urls):
    """Count number of url by host"""
    stats = {}
    for url in urls:
        if url.startswith('https://'):
            proto = 'https://'
            url = url[8:]
        if url.startswith('http://'):
            proto = 'http://'
            url = url[7:]
        host = proto + url.split('/')[0]
        if host in stats.keys():
            stats[host] = stats[host] + 1
        else:
            stats[host] = 1
    st_sorted = sorted(stats.iteritems(), key=operator.itemgetter(1), reverse=True)
    return st_sorted 

def store_html(html, fpath):
    """Store data in file"""
    fout = open(fpath,'w')
    fout.write(html)
    fout.close()

def is_image(url):
    result = False
    if url.endswith('.jpg'):
        result = True
    elif url.endswith('.png'):
        result = True
    elif url.endswith('.gif'):
        result = True
    return result

def is_style(url):
    result = False
    if url.endswith('.css'):
        result = True
    return result

def is_script(url):
    result = False
    if url.endswith('.js'):
        result = True
    return result


def stats_for_host(host, urls):
    """Count number of url by host"""
    images, scripts, styles = 0, 0, 0
    for url in urls:
        if url.startswith(host):
            if is_image(url):
                images = images + 1
            elif is_script(url):
                scripts = scripts + 1
            elif is_style(url):
                styles = styles + 1
    return images, scripts, styles


def main():
    """Main programm"""
    options = arg_parse()
    if options.url != None and options.hostname == None:
        options.hostname = options.url
    hostname = options.hostname
    check_options(options)
    total = 0

    credentials = [ options.username, options.password ]

    if not hostname.endswith('/'):
        hostname = hostname + '/'

    if options.infile != None:
        if not os.path.exists(options.infile):
            sys.exit(1)
        else:
            # parse file and return nodes
            html = parse_file(options.infile)

    if options.url != None:
        html = parse_url(options.url, credentials)

    if options.outfile != None:
        store_html(html, options.outfile)

    [images, scripts, styles] = parse(html)
        
    #print "%d %d %d" % (len(images), len(styles), len(scripts))

    size_img, no_cache_img = analyze_urls(images, hostname, credentials, options.exclusive, "img")
    size_scr, no_cache_scr = analyze_urls(scripts, hostname, credentials, options.exclusive, "script")
    size_sty, no_cache_sty = analyze_urls(styles, hostname, credentials, options.exclusive, "styl")

    total = size_img + size_scr + size_sty
    total_no_cache = no_cache_img + no_cache_scr + no_cache_sty

    print """\nElements stats"""
    print """----------------"""
    print """  sizes :  cached / not ca."""
    print """images  : %7s / %7s ( %d / %d )""" % (pretty_size(size_img),
                                                   pretty_size(no_cache_img),
                                                   size_img, no_cache_img)
    print """scripts : %7s / %7s ( %d / %d )""" % (pretty_size(size_scr),
                                                   pretty_size(no_cache_scr),
                                                   size_scr, no_cache_scr)
    print """styles  : %7s / %7s ( %d / %d )""" % (pretty_size(size_sty),
                                                   pretty_size(no_cache_sty),
                                                   size_sty, no_cache_sty)
    print """ Total  : %7s / %7s ( %d / %d )""" % (pretty_size(total), 
                                                   pretty_size(total_no_cache),
                                                   total, 
                                                   total_no_cache)

    urls = []
    for url in images + scripts + styles:
        urls.append( completeurl(url, hostname) )
        
    print """\nHosts stats"""
    print """Tot  (I) (J) (S)"""
    print """------------------"""
    for datas in hosts_stats(urls):
        img, scr, sty = stats_for_host(datas[0], urls)
        print "%3d %3d %3d %3d : %s" % (datas[1], img, scr, sty, datas[0])
    print """(I) : images, (J) scripts, (S) styles"""

if __name__ == '__main__':

    main()
