#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Cornelius fetch an url, looks in html data all sub-urls and show
information about each of them, like cache age, size

"""
import sys
import os
from optparse import OptionParser
from BeautifulSoup import BeautifulSoup
import urllib2
import time
import operator

__version__ = "1.1.0"

class NoRedirection(urllib2.HTTPErrorProcessor):

    def http_response(self, request, response):
        code, msg, hdrs = response.code, response.msg, response.info()
        
        return response

    https_response = http_response

def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-i FILENAME] [-u URL] [-avs]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-a", "--all", dest="exclusive",
                      action="store_false",
                      help="check all urls",
                      default=True)
    parser.add_option("-u", "--url", dest="url",
                      help="url to fetch and analyse",
                      default=None)
    parser.add_option("-i", "--infile", dest="infile",
                      help="input file",
                      default=None)
    parser.add_option("--hostname", dest="hostname",
                      help="hostname like http://fqdn/",
                      default=None)
    parser.add_option("--username", dest="username",
                      help="username for HTTP auth",
                      default=None)
    parser.add_option("-O", "--outfile", dest="outfile",
                      help="dumpt html in outfile",
                      default=None)
    parser.add_option("--password", dest="password",
                      help="password for HTTP auth",
                      default=None)
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose",
                      default=False)

    return parser.parse_args()[0]

def check_options(options):
    """Check mandatory options"""
    if options.infile == None and options.url == None:
        print """Input file or url is required, use -i or -u on command line"""
        sys.exit(1)

    if options.infile != None and options.hostname == None:
        print """You must add --hostname option when using input file"""
        sys.exit(1)

    if not options.hostname.startswith('http://') and not options.hostname.startswith('https://'):
        print """Hostname must start with http or https"""
        sys.exit(2)

def urlparse(url, credentials):
    """
    open an url and return information
    """
    username, password = credentials
    if username != None and password != None:
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, url, username, password)
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        # create "opener" (OpenerDirector instance)
        opener = urllib2.build_opener(handler)
        opener.open(url)
        urllib2.install_opener(opener)

    try:
        response = urllib2.urlopen(url)
    except urllib2.HTTPError, err:
        print err

    return response.info()

def parse_url(url, credentials):
    """Parse an online document"""
    html = urlfetch(url, credentials)
    if html == None:
        print 'No html code to analyze'
        sys.exit(4)
    return url

def urlfetch(url, credentials):
    """
    Fetch an url, return body part
    """
    username, password = credentials

    if username != None and password != None:
        password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None, url, username, password)
        handler = urllib2.HTTPBasicAuthHandler(password_mgr)
        # create "opener" (OpenerDirector instance)
        opener = urllib2.build_opener(handler)
        opener.open(url)
        urllib2.install_opener(opener)

    try:

        opener = urllib2.build_opener(NoRedirection)
        response = opener.open(url)
        #response = urllib2.urlopen(url)
    except urllib2.HTTPError, error:
        print error
        sys.exit(1)
    try:
        html = response.read()
    except:
        html = None

    code = response.getcode()
    header = response.info()
    if code == 200:
        print "%d : %s" % (code, url)
    elif code == 301:
        print "%d : %s - %s" % (code, url, header.getheader('Location'))
    elif code == 302:
        print "%d : %s - %s" % (code, url, header.getheader('Location'))
    
    return html


def main():
    """Main programm"""
    options = arg_parse()
    if options.url != None and options.hostname == None:
        options.hostname = options.url
    hostname = options.hostname
    check_options(options)
    total = 0

    credentials = [ options.username, options.password ]

    if not hostname.endswith('/'):
        hostname = hostname + '/'

    if options.infile != None:
        if not os.path.exists(options.infile):
            sys.exit(1)
        else:
            # parse file and return nodes
            html = parse_file(options.infile)

    if options.url != None:
        html = parse_url(options.url, credentials)

    if options.outfile != None:
        store_html(html, options.outfile)


if __name__ == '__main__':

    main()
