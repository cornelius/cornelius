#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
import unittest
import urllib2
sys.path.append('./')

from cornelius import *

class TestsCornelius(unittest.TestCase):
    """Main parts"""
    def test_pretty_size_a(self):
        """Configuration file"""
        size = pretty_size(2048)
        self.assertEqual(size, '2.00K', 'Pretty size : %s' % (size))

    def test_pretty_size_b(self):
        size = pretty_size(2049)
        self.assertEqual(size, '2.00K', 'Pretty size : %s' % (size))

    def test_pretty_size_c(self):
        size = pretty_size(1024*1024)
        self.assertEqual(size, '1.00M', 'Pretty size : %s' % (size))

    def test_pretty_size_d(self):
        size = pretty_size(256)
        self.assertEqual(size, '256b', 'Pretty size : %s' % (size))

    def test_sort_urls_a(self):
        foobar = sort_urls(['foo.gif', 'foo.css', 'foo.js', 'foo.jpeg'])
        self.assertEqual(foobar[0], 'foo.gif', 'Sort urls')
        self.assertEqual(foobar[1], 'foo.jpeg', 'Sort urls')
        self.assertEqual(foobar[2], 'foo.js', 'Sort urls')
        self.assertEqual(foobar[3], 'foo.css', 'Sort urls')

    def test_completeurl_a(self):
        foobar = completeurl('http://www.foo.eu', 'http://www.foo.eu')
        self.assertEqual(foobar, 'http://www.foo.eu', 'Complete_url')

    def test_completeurl_b(self):
        foobar = completeurl('/img/foo.png', 'http://www.foo.eu')
        self.assertEqual(foobar, 'http://www.foo.eu/img/foo.png', 'Complete_url')

    def test_completeurl_c(self):
        foobar = completeurl('img/foo.png', 'http://www.foo.eu')
        self.assertEqual(foobar, 'http://www.foo.eu/img/foo.png', 'Complete_url : ' + foobar)

    def test_completeurl_d(self):
        foobar = completeurl('img', 'http://www.foo.eu')
        self.assertEqual(foobar, 'http://www.foo.eu/img', 'Complete_url')

    def test_completeurl_e(self):
        foobar = completeurl('http://www.foobar.eu/', 'http://www.foo.eu')
        self.assertEqual(foobar, 'http://www.foobar.eu/', 'Complete_url')

    def test_completeurl_f(self):
        foobar = completeurl('https://www.foobar.eu/', 'https://www.foo.eu')
        self.assertEqual(foobar, 'https://www.foobar.eu/', 'Complete_url')

    def test_completeurl_g(self):
        foobar = completeurl('img/foo.png', 'https://www.foo.eu')
        self.assertEqual(foobar, 'https://www.foo.eu/img/foo.png', 'Complete_url : ' + foobar)

    def test_parse_a(self):
        foobar = parse('<html><body></body></html>')
        self.assertEqual(foobar, [[],[],[]], 'Parse empty')

    def test_parse_b(self):
        foobar = parse('<html><body><img src="/foo.png"></body></html>')
        self.assertEqual(foobar, [['/foo.png'],[],[]], 'Parse : ')

    def test_parse_c(self):
        foobar = parse('<html><body><img src="/foo.png"><img src="foobar.jpeg"></body></html>')
        self.assertEqual(foobar, [['/foo.png','foobar.jpeg'],[],[]], 'Parse : ')

    def test_parse_d(self):
        foobar = parse('<html><body><img src="bar.png"><script src="foo.js"></body></html>')
        self.assertEqual(foobar, [['bar.png'],['foo.js'],[]], 'Parse : ')

    def test_parse_e(self):
        foobar = parse('<html><body><img src="bar.png"><script></script></body></html>')
        self.assertEqual(foobar, [['bar.png'],[],[]], 'Parse : ')

    def test_parse_f(self):
        foobar = parse('<html><body><img src="bar.png"><link href="bar.css" rel="stylesheet"></link></body></html>')
        self.assertEqual(foobar, [['bar.png'],[],['bar.css']], 'Parse : ')

    def test_parse_g(self):
        foobar = parse('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><body><p><img src="bar.png"></p><link rel="stylesheet" href="bar.css"></link></body></html>')
        self.assertEqual(foobar, [['bar.png'],[],['bar.css']], 'Parse : ')

    def test_parse_h(self):
        foobar = parse('<html><head><style type="text/css" media="screen">@import url(/themes/style.css);</style></head><body><img src="bar.png"></body></html>')   
        self.assertEqual(foobar, [['bar.png'],[],['/themes/style.css']], 'Parse styles returns : ' + str(foobar))

    def test_parse_i(self):
        foobar = parse('<html><body><p><img src="bar.png"></p><link href="bar.css"></link></body></html>')
        self.assertEqual(foobar, [['bar.png'],[],[]], 'Parse : ')

    def test_css_import_a(self):
        foobar = css_import('@import url(/themes/style.css);')   
        self.assertEqual(foobar, '/themes/style.css', 'css_import returns : ' + str(foobar))

    def test_hosts_stats_a(self):
        foobar = hosts_stats(['http://foo.fr/bar','http://foo.fr/foo', 'http://bar.eu/foo'])
        self.assertEqual(foobar, [('http://foo.fr', 2),('http://bar.eu', 1)], 'hosts_stats')

    def test_hosts_stats_a(self):
        foobar = hosts_stats(['http://bar.eu/foo', 'http://foo.fr/bar','http://foo.fr/foo'])
        self.assertEqual(foobar, [('http://foo.fr', 2),('http://bar.eu', 1)], 'hosts_stats')

    def test_is_image_a(self):
        self.assertEqual(True, is_image('http://foo.fr/foo.png'), 'hosts_stats')

    def test_is_image_b(self):
        self.assertEqual(True, is_image('http://foo.fr/foo.jpg'), 'hosts_stats')

    def test_is_image_c(self):
        self.assertEqual(True, is_image('http://foo.fr/foo.gif'), 'hosts_stats')

    def test_is_image_d(self):
        self.assertEqual(False, is_image('http://foo.fr/foopng'), 'hosts_stats')

    def test_is_style_a(self):
        self.assertEqual(True, is_style('http://foo.fr/foo.css'), 'hosts_stats')

    def test_is_style_b(self):
        self.assertEqual(False, is_style('http://foo.fr/foocss'), 'hosts_stats')

    def test_is_script_a(self):
        self.assertEqual(True, is_script('http://foo.fr/foo.js'), 'hosts_stats')

    def test_is_script_b(self):
        self.assertEqual(False, is_script('http://foo.fr/foocss'), 'hosts_stats')

    def test_stats_for_host_a(self):
        urls = ['http://foo.fr/foo.css', 'http://foo.fr/bar.css', 
                'http://foo.fr/foo.png', 'http://bar.fr/bar.css' ]
        self.assertEqual((1,0,2), stats_for_host('http://foo.fr', urls), 'hosts_stats')

    def test_stats_for_host_b(self):
        urls = ['http://foo.fr/foo.css', 'http://foo.fr/bar.css', 
                'http://foo.fr/foo.png', 'http://bar.fr/bar.css' ]
        self.assertEqual((0,0,0), stats_for_host('http://foobar.fr', urls), 'hosts_stats')


if __name__ == '__main__':
    unittest.main()
